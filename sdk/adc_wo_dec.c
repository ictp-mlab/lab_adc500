#include <stdio.h>
#include "xil_printf.h"
#include "comblock.h"
#include "adc500.h"
#include "xparameters.h"

#define adc_cfg led0|led1
int main()
{
	u32 val;

    //ADC INIT
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG0, adc_cfg);

//    Clear FIFO
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 1);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 0);

    while (1){
    for (int i=0; i<1000; i++){
    	val=cbRead(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_VALUE);
//    	xil_printf("%d,", (val>>15&0x1fffe));
    	xil_printf("%d\n\r", val&0xff);
//    	xil_printf("%d,", (val>>15&0x1fffe)+1);
    	xil_printf("%d\n\r", val>>8&0xff);
    }
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 1);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 0);
    }

    return 0;
}
